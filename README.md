mat2sng
=======

Generates `sng` files from binary dumps of PETSc matrices in an pixel-per-element basis.
These files can then be converted to `png` using the tool `sng` <http://sng.sourceforge.net>:

    $ ./mat2sng matrix.bin | sng > matrix.png
    
    
code is copyright (C) 2015 jeremy theler  
code is licensed under [GNU GPL version 3](http://www.gnu.org/copyleft/gpl.html) or (at your option) any later version.  
code is free software: you are free to change and redistribute it.  
There is NO WARRANTY, to the extent permitted by law.  
