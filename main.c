/* mat2sng
 * author: jeremy theler <gtheler@cites-gss.com>
 * license: GPL v3+
 */
static char help[] = "\
mat2sng\n\
usage: ./mat2sng <matrix file>\n\n\
converts a PETSc binary matrix file into an SNG (scriptable network graphics)\n\
file which can be converted into a PNG image containing a representation of\n\
the non-zero structure in a pixel-by-element correspondence (i.e a MxN matrix\n\
results in an MxN image)\n\
\n\
the standard output should be fed to the tool sng <http://sng.sourceforge.net>:\n\
\n\
    $ ./mat2sng matrix.bin | sng > matrix.png\n\n";

#include <petscsys.h>
#include <petscmat.h>

#include "mat2sng.h"

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  Mat               A;
  PetscViewer       fd;
  char              file[PETSC_MAX_PATH_LEN];
  PetscMPIInt       rank;

  PetscInitialize(&argc, &argv, (char*)0, help);
  petsc_call(MPI_Comm_rank(PETSC_COMM_WORLD, &rank));

  if (argc < 2 || (argc > 1 && argv[1][0] == '-')) {
    petsc_call(PetscPrintf(PETSC_COMM_WORLD, "%s", help));
    return 0;
  }
  petsc_call(PetscStrncpy(file, argv[1], PETSC_MAX_PATH_LEN-1));
  
  petsc_call(PetscViewerBinaryOpen(PETSC_COMM_WORLD, file, FILE_MODE_READ, &fd));
  petsc_call(MatCreate(PETSC_COMM_WORLD, &A));
  petsc_call(MatSetFromOptions(A));
  petsc_call(MatLoad(A, fd));

  petsc_call(mat2sng(A, 1024, 0, 1, PETSC_VIEWER_STDOUT_WORLD));
   
  petsc_call(MatDestroy(&A));
  petsc_call(PetscViewerDestroy(&fd));

  petsc_call(PetscFinalize());
    
  return 0;
}
