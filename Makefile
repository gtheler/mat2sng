include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

all:
	${CC} -g -c mat2sng.c -Wall ${PETSC_CC_INCLUDES}
	${CC} -g -c main.c -Wall ${PETSC_CC_INCLUDES}
	${CLINKER} -o mat2sng main.o mat2sng.o ${PETSC_LIB}
